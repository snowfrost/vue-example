import Vue from 'vue';

import {VueExtender} from '../core/vue-extender'

import * as tabsHead from './components/tabs-head.vue';
import * as tabsBody from './components/tabs-body.vue';
import * as modalView from './components/modal-view.vue';



export class Calculator extends Vue {

  constructor(data) {

    data.message.show = (data.message.show === '1')

    data.tabs = _.each(data.tabs, function (tab) {
      tab.isCalc= (tab.isCalc === '1')
      _.each(tab.blocks, function (block) {
        block.hasData = (block.hasData === '1')
        block.open = (block.open === '1')
      });
    });

    super({
      data : data,
      el: '#main-calc',
      components: Calculator.components,
      methods: Calculator.methods,
    });
  }

  static components = {
    'tabs-head' :tabsHead,
    'tabs-body' :tabsBody,
    'modal-view' : modalView
  };

  static  methods = {
    test : function() {

    }
  }
}