import Vue from 'vue'

export class VueExtender {
  static extend(params) {
    return Vue.extend(params)
  }
}