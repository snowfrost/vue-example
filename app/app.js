'use strict';

// Load plugins
import 'jquery';
import 'normalize.css';
require('perfect-scrollbar/jquery')($);
// Load styles
import 'swiper/dist/css/swiper.css';
import _ from 'underscore';



// load modules
import {TemplateContainer} from './js/template-container'
//import {MainContainer} from './js/main-container'

import {Calculator} from './js/calculator'
window.Calculator = Calculator;


