'use strict';

var EncodingPlugin = require('webpack-encoding-plugin');
var params = {
  env: process.env.NODE_ENV || 'development'
};

// Require local webpack
var webpack = require('webpack');

// Externals plugins
var plugins = {
  bower: require("bower-webpack-plugin"),
  extText: require("extract-text-webpack-plugin"),
  cssOptimize: require('optimize-css-assets-webpack-plugin')

};


// PostCss plugins

var postCssPlugins = {
    autoprefixer: require("autoprefixer"),
    postcssImport: require('postcss-import'),
    postcssMixins: require('postcss-mixins'),
    nested: require('postcss-nested'),
    customProperties: require("postcss-custom-properties"),
    postcssEach: require("postcss-each"),
    postcssFor: require("postcss-for"),
    postcssMedia: require("postcss-custom-media"),
    postcssMediaNested: require("postcss-nesting"),
    posctcssFlexBugFix: require("postcss-flexbugs-fixes"),
    postcssInput: require("postcss-input-style"),
    postcssExtend: require("postcss-extend"),
    postcssObjFit: require("postcss-object-fit-images"),
    postcssPixToRem: require("postcss-pxtorem"),
    postcssNesting: require("postcss-nesting"),
    postcssGradientTransparencyFix: require('postcss-gradient-transparency-fix'),
    mqpacker: require("css-mqpacker")
};

module.exports = {
  context: __dirname + "/app",
  entry: './app',
  stats: { children: false },

  // Watcher
  watch: params.env == 'development',
  watchOptions: {
    aggregateTimeout: 50
  },

  // Resolve params
  resolve: {
    root: __dirname,
    modulesDirectories: [
      __dirname,
      __dirname + '/vendor',
      __dirname + '/node_modules'
    ],
    packageFiles: ['package.json', 'bower.json', '.bower.json'],
    extensions: ['', '.js', '.vue',  '.css'],
    alias: {
      'vue': 'vue/dist/vue.js',
    },

  },

  resolveLoader: {
    root: __dirname,
    modulesDirectories: [__dirname + "/node_modules"]
  },

  // Output params
  output: {
    path: __dirname + '/build',
    publicPath: '/local/client/build/',
    filename: 'app.js',
    library: '[name]'
  },

  // Loaders params
  module: {
    loaders: [
      {
        test: require.resolve('jquery'), loader: 'expose?jQuery!expose?$'
      },
      {
        test: /\.vue$/,
        loader: 'vue',
        exclude: /(node_modules|vendor)/,

      },
      {
        test: /\.css$/,
        loader: plugins.extText.extract('style-loader', '!css-loader!postcss-loader')
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|vendor)/,
        loader: 'babel',
        query: {
          cacheDirectory: true,
          plugins: ['transform-decorators-legacy'],
          presets: [
            require.resolve('babel-preset-es2015'),
            require.resolve('babel-preset-stage-0')
          ]
        }
      }, {
        test: /\.jpe?g$|\.gif$|\.png$|\.woff$|\.eot$|\.svg$|\.ttf$|\.wav$|\.mp3$/,
        loader: "file?name=[path][name].[ext]",
        exclude: /(icons)/
      },
    ]
  },

   postcss: function () {
       return [
           postCssPlugins.postcssImport({
               addDependencyTo: webpack
           }),
           postCssPlugins.customProperties,
           postCssPlugins.postcssMixins,
           postCssPlugins.nested,
           postCssPlugins.postcssNesting,
           postCssPlugins.postcssExtend,
           postCssPlugins.posctcssFlexBugFix,
           postCssPlugins.postcssEach,
           postCssPlugins.postcssFor,
           postCssPlugins.postcssInput,
           postCssPlugins.postcssMedia,
           postCssPlugins.postcssGradientTransparencyFix,
           postCssPlugins.autoprefixer,
           postCssPlugins.postcssObjFit
       ];
   },

    // Plugins
  plugins: [
    new plugins.extText('[name].css', {
      allChunks: true
    }),
    new webpack.ProvidePlugin({
      _: "underscore",
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new EncodingPlugin({
      encoding: 'windows-1251'
    })
  ]
};

if (params.env == 'production') {

  module.exports.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          // don't show unreachable variables etc
          warnings:     false,
          drop_console: true,
          unsafe:       true
        }
      })
  );

  module.exports.plugins.push(

      new plugins.cssOptimize({
          cssProcessor: require('cssnano'),
          cssProcessorOptions: {
              discardDuplicates: true
          },
      })
  );
}
